package com.example.quiz_7.fragments


import android.util.Log
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.quiz_7.Resource
import com.example.quiz_7.User
import com.example.quiz_7.UserManager
import com.example.quiz_7.UserViewModel
import com.example.quiz_7.abstracts.BaseFragment
import com.example.quiz_7.databinding.FragmentLogInBinding
import com.example.quiz_7.databinding.FragmentRegisterBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class RegisterFragment : BaseFragment<FragmentRegisterBinding>(FragmentRegisterBinding::inflate) {
    lateinit var userManager: UserManager
    override fun init() {
        userManager = UserManager(context)
        listeners()
    }

    private val viewModel: UserViewModel by viewModels()

    private fun listeners() {
        binding.btnRegister.setOnClickListener {
            sendLogin()
        }
    }

    private fun sendLogin() {
        viewModel.loginUser(binding.etEmail.text.toString(), binding.etPassword.text.toString())
        viewModel.loginResponse.observe(viewLifecycleOwner, {
            when (it) {
                is Resource.Loading -> {
                    Log.d("LOG", "Loading")
                }
                is Resource.Success -> {
                    saveUser(User(token = it.data!!.token, id = it.data!!.id))
                    findNavController().navigate(LogInFragmentDirections.actionLogInFragmentToHomeFragment2())
                    Log.d("LOG", "success")
                }
                is Resource.Error -> {
                    Log.d("LOG", "Loading")
                }
            }
        })
    }

    private fun saveUser(user: User) {
        //  Stores the values
        viewLifecycleOwner.lifecycleScope.launch {
            withContext(Dispatchers.IO) {
        //  write()
                userManager.storeUserDataStore(
                    User(
                        token = user.token, id = user.id
                    )
                )
            }
        }
    }
}
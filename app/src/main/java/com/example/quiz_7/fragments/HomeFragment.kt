package com.example.quiz_7.fragments

import android.util.Log
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.quiz_7.Resource
import com.example.quiz_7.UserManager
import com.example.quiz_7.UserViewModel
import com.example.quiz_7.abstracts.BaseFragment
import com.example.quiz_7.databinding.FragmentHomeBinding
import com.example.quiz_7.User
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class HomeFragment : BaseFragment<FragmentHomeBinding>(FragmentHomeBinding::inflate) {
    lateinit var userManager: UserManager
    private val viewModel: UserViewModel by viewModels()

    override fun init() {
        userManager = UserManager(context)
        observeData()
        listeners()
    }

    private fun listeners() {
        binding.btnLogout.setOnClickListener {
            openLogin()
            killSession()
        }
    }

    private fun killSession() {
        viewLifecycleOwner.lifecycleScope.launch {
            withContext(IO) {
                //                write()
                userManager.storeUserDataStore(
                    User(
                        token = "", id = 0
                    )
                )
            }
        }
    }

    private fun openLogin() {
        findNavController().navigate(
            HomeFragmentDirections.actionHomeFragmentToRegisterFragment()
        )
    }

    private fun observeData() {
        viewLifecycleOwner.lifecycleScope.launch {
            userManager.getUserDataStore().catch { e ->
                Log.d("catch", "$e")
            }.collect {
                withContext(IO) {
                    //  token = it.token.toString()
                    if (!it.token.isNullOrEmpty() && it.id != 0) {
                        loadUserVM(it.id!!.toInt())
                    } else {
                        openLogin()
                    }
                }
            }
        }
    }

    private fun loadUserVM(id: Int) {
        viewModel.loadUser(id)
        userObserve()

    }

    private fun userObserve() {
        viewModel.userResponse.observe(viewLifecycleOwner, {
            when (it) {
                is Resource.Loading -> {
                    Log.d("LOG", "Loading")
                }
                is Resource.Success -> {
                    Log.d("LOG", "Success")
                    binding.email.text = it.data!!.email
                }
                is Resource.Error -> {
                    Log.d("LOG", "Success")
                }
            }
        })
    }
}


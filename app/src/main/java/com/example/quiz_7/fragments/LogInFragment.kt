package com.example.quiz_7.fragments


import android.util.Log
import android.util.Log.d
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.quiz_7.Resource
import com.example.quiz_7.User
import com.example.quiz_7.UserManager
import com.example.quiz_7.UserViewModel
import com.example.quiz_7.abstracts.BaseFragment
import com.example.quiz_7.databinding.FragmentLogInBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class LogInFragment : BaseFragment<FragmentLogInBinding>(FragmentLogInBinding::inflate) {
    lateinit var userManager: UserManager
    override fun init() {
        userManager = UserManager(context)
        listeners()
    }

    private val viewModel: UserViewModel by viewModels()

    private fun listeners() {
        binding.btnLogin.setOnClickListener {
            sendLogin()
            findNavController().navigate(LogInFragmentDirections.actionLogInFragmentToHomeFragment2())
        }
    }

    private fun sendLogin() {
        viewModel.loginUser(binding.etEmail.text.toString(), binding.etPassword.text.toString())
        viewModel.loginResponse.observe(viewLifecycleOwner, {
            when (it) {
                is Resource.Loading -> {
                    d("LOG", "Loading")
                }
                is Resource.Success -> {
                    saveUser(User(token = it.data!!.token, id = it.data!!.id))
                    d("LOG", "success")
                }
                is Resource.Error -> {
                    d("LOG", "Loading")
                }
            }
        })


    }

    private fun saveUser(user: User) {
        //Stores the values
        viewLifecycleOwner.lifecycleScope.launch {
            withContext(Dispatchers.IO) {
                //    write()
                userManager.storeUserDataStore(
                    User(
                        token = user.token, id = user.id
                    )
                )
            }
        }
    }

    private fun observeData() {
        viewLifecycleOwner.lifecycleScope.launch {
            userManager.getUserDataStore().catch { e ->
                Log.d("catch", "$e")
            }.collect {
                withContext(Dispatchers.IO) {
                    //  token = it.token.toString()
                    if (!it.token.isNullOrEmpty() && it.id != 0) {
                        findNavController().navigate(LogInFragmentDirections.actionLogInFragmentToHomeFragment2())
//                        loadUserVM(it.id!!.toInt())
                    }
                }
            }
        }
    }
}
package com.example.quiz_7

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.flow.map


private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "settings")

class UserManager(context: Context?) {

    private val dataStore: DataStore<Preferences> = context!!.dataStore

    companion object {

        val USER_EMAIL = stringPreferencesKey("USER_EMAIL")
        val USER_ID = intPreferencesKey("USER_ID")
        val USER_TOKEN = stringPreferencesKey("USER_TOKEN")

    }

    suspend fun storeUserDataStore(user: User) {
        dataStore.edit {
//            it[USER_EMAIL] = user.email.toString()
            it[USER_TOKEN] = user.token.toString()
            it[USER_ID] = user.id.toString().toInt()
        }
    }

    fun getUserDataStore() = dataStore.data.map {
        User(
//            email = it[USER_EMAIL] ?: "",
            token = it[USER_TOKEN] ?: "",
            id = it[USER_ID] ?: 0,
        )
    }
}
package com.example.quiz_7.networking

import com.example.quiz_7.Constants.Companion.LOGIN
import com.example.quiz_7.Constants.Companion.REGISTER
import com.example.quiz_7.Constants.Companion.USERS
import com.example.quiz_7.User
import retrofit2.Response
import retrofit2.http.*


interface ApiService {

    @POST(LOGIN)
    suspend fun loginUser(@Body params: User): Response<User>


    @POST(REGISTER)
    suspend fun registerUser(@Body params: User): Response<User>

    @Headers("User-Agent: Retrofit-Sample-App")
    @POST(USERS)
    suspend fun getUser(@Path("id") id: Int): Response<User>

}


//    @Headers("User-Agent: Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11")
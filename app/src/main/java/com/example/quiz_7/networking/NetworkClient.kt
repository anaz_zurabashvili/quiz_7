package com.example.quiz_7.networking

import com.example.quiz_7.Constants.Companion.BASE_URL
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object NetworkClient {
    private var retrofit = Retrofit.Builder()
        .addConverterFactory(
            MoshiConverterFactory
            .create(
                Moshi.Builder()
                    .addLast(KotlinJsonAdapterFactory())
                    .build()
            ))
        .baseUrl(BASE_URL)
        .build()

    var service: ApiService = retrofit.create(ApiService::class.java)
}

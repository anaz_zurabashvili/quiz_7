package com.example.quiz_7

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.quiz_7.networking.NetworkClient
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class UserViewModel : ViewModel() {

    private var _userResponse = MutableLiveData<Resource<User>>()
    val userResponse: LiveData<Resource<User>>
        get() = _userResponse

    private var _registerResponse = MutableLiveData<Resource<User>>()
    val registerResponse: LiveData<Resource<User>>
        get() = _registerResponse

    private var _loginResponse = MutableLiveData<Resource<User>>()
    val loginResponse: LiveData<Resource<User>>
        get() = _loginResponse

    fun loadUser(id: Int) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                getUserId(id)
            }
        }
    }
    fun registerUser(id: Int) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                getUserId(id)
            }
        }
    }
    fun loginUser(email: String, password: String) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                loginUserR(email, password)
            }
        }
    }

    private suspend fun registerUser(email: String, password: String) {
        _registerResponse.postValue(Resource.Loading())
        try {
            val result = NetworkClient.service.registerUser(User(id = null, email= email, password = password, token = null))
            val body = result.body()
            _registerResponse.postValue(
                if (result.isSuccessful && body != null)
                    Resource.Success(body)
                else
                    Resource.Error(message = result.message().toString())
            )
        } catch (e: Exception) {
            _registerResponse.postValue(Resource.Error(message = e.toString()))
        }
    }

    private suspend fun loginUserR(email: String, password: String) {
        _loginResponse.postValue(Resource.Loading())
        try {
            val result = NetworkClient.service.loginUser(User(id = null, email= email, password = password, token = null))
            val body = result.body()
            _loginResponse.postValue(
                if (result.isSuccessful && body != null)
                    Resource.Success(body)
                else
                    Resource.Error(message = result.message().toString())
            )
        } catch (e: Exception) {
            _loginResponse.postValue(Resource.Error(message = e.toString()))
        }
    }

    private suspend fun getUserId(id: Int) {
        _userResponse.postValue(Resource.Loading())
        try {
            val result = NetworkClient.service.getUser(id)
            val body = result.body()
            body
            _userResponse.postValue(
                if (result.isSuccessful && body != null) {
                    Log.d("anuki", "success")
                    Resource.Success(body)
                } else {
                    Log.d("anuki", "error")
                    Resource.Error(message = result.message().toString())
                }
            )
        } catch (e: Exception) {
            Log.d("anuki", "catch")
            _userResponse.postValue(Resource.Error(message = e.toString()))
        }
    }

}

package com.example.quiz_7

data class User(
    var id: Int? = null,
    var token:String? = null,
    var email:String? = null,
    var password:String? = null,

)
